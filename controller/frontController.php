<?php

Class frontController Extends baseController {

    public function index() {
        $this->registry->template->page_heading = 'This is the page Index';
        $this->registry->template->show('page_index');
    }

    public function TellMeDate() {
        $this->registry->template->datentime = date('Y/m/d H:i:s');
        //return $date;
    }

    public function autocomplete() {
        //$data = array("alpaca", "buffalo", "cat", "tiger");
        //echo json_encode($data);
        if(!isset($_GET['keyword'])){
            die();
        }
        $keyword = $_GET['keyword'];
        //debug_to_console("got here - " . $keyword);
        $data = Artist::searchByArtist($keyword);
        echo json_encode($data);
        
    }

}

?>