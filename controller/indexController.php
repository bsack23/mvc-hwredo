<?php
//http://www.phpro.org/tutorials/Model-View-Controller-MVC.html
Class indexController Extends baseController {

public function index() {
	/*** set a template variable ***/
        $this->registry->template->head_title = 'Hallwalls :: Current and Upcoming';
        
        $this->registry->template->header_menu = __SITE_PATH . "/views/header_menu.php";
        $this->registry->template->header_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_prog_nav = __SITE_PATH . "/views/prog_nav.php";
        $this->registry->template->social_crap = __SITE_PATH . "/views/social_crap.php";
        /*** make an array of all upcoming events ***/
        $this->registry->template->slides = Slideshow::getAll();
        $this->registry->template->events = Event::getAll();
        $this->registry->template->gallery = Event::getGallery();
        $this->registry->template->timemachine = Event::getTimemachine();
        $this->registry->template->news = News::getAll();
	
        /*** load the index templates ***/
        $this->registry->template->show('index_head');
        $this->registry->template->show('index_content');
        $this->registry->template->show('index_sidebar');
        $this->registry->template->show('footer');
}

}

?>
