<?php

//http://www.phpro.org/tutorials/Model-View-Controller-MVC.html
Class pageController Extends baseController {

    public function index() {
        $this->registry->template->page_heading = 'This is the page Index';
        $this->registry->template->show('page_index');
    }

    public function event($event_id = '') {
        $this->registry->template->header_menu = __SITE_PATH . "/views/header_menu.php";
        $this->registry->template->header_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_prog_nav = __SITE_PATH . "/views/prog_nav.php";
        $this->registry->template->social_crap = __SITE_PATH . "/views/social_crap.php";
        //if (!isset($_GET['id'])) {
        //    return call('pages', 'error');
        //}
        //get specified event
        $this->registry->template->event = Event::getEvent($event_id);
        //get artists linked to this event
        $this->registry->template->artists = Artist::getEventArtists($event_id);
        //get publications linked to this event
        $this->registry->template->pubs = Pub::getEventPubs($event_id);
        /*         * * load the page templates ** */
        $this->registry->template->show('event_head');
        $this->registry->template->show('event_content');
        $this->registry->template->show('event_sidebar');
        $this->registry->template->show('footer');
    }

    public function program($program_id = '') {
        $this->registry->template->header_menu = __SITE_PATH . "/views/header_menu.php";
        $this->registry->template->header_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_prog_nav = __SITE_PATH . "/views/prog_nav.php";
        $this->registry->template->social_crap = __SITE_PATH . "/views/social_crap.php";
        $this->registry->template->title = Event::getProgramTitle($program_id);
        $this->registry->template->program = Event::getProgram($program_id);
        /*         * * load the page templates ** */
        $this->registry->template->show('page_head');
        $this->registry->template->show('program_content');
        //$this->registry->template->show('event_content');
        //$this->registry->template->show('event_sidebar');
        $this->registry->template->show('footer');
    }

    public function publication($pub) {
        $this->registry->template->header_menu = __SITE_PATH . "/views/header_menu.php";
        $this->registry->template->header_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_prog_nav = __SITE_PATH . "/views/prog_nav.php";
        $this->registry->template->social_crap = __SITE_PATH . "/views/social_crap.php";
        $this->registry->template->pub = Pub::getPub($pub);
        $this->registry->template->artists = Artist::getPubArtists($pub);
        $this->registry->template->show('page_head');
        $this->registry->template->show('pub_content');
        //$this->registry->template->show('artist_sidebar');
        $this->registry->template->show('footer');
        
    }

    public function search() {
        $this->registry->template->header_menu = __SITE_PATH . "/views/header_menu.php";
        $this->registry->template->header_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_prog_nav = __SITE_PATH . "/views/prog_nav.php";
        $this->registry->template->social_crap = __SITE_PATH . "/views/social_crap.php";
        if (isset($_POST['search_string'])) {
            $terms = $_POST['search_string'];
            //$terms = filter_input(INPUT_POST, 'search_string');
            $this->registry->template->results = Search::getEverything($terms);
        }
        $this->registry->template->title = "Search";
        $this->registry->template->show('page_head');
        $this->registry->template->show('search_content');
        $this->registry->template->show('footer');
    }

    public function artist($artist_id = '') {
        $this->registry->template->header_menu = __SITE_PATH . "/views/header_menu.php";
        $this->registry->template->header_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_navselect = __SITE_PATH . "/views/navselect.php";
        $this->registry->template->footer_prog_nav = __SITE_PATH . "/views/prog_nav.php";
        $this->registry->template->social_crap = __SITE_PATH . "/views/social_crap.php";
        //if (!isset($_GET['id'])) {
        //    return call('pages', 'error');
        //}
        $pieces = explode("-", $artist_id);
        $this->registry->template->artist = Artist::getArtist($pieces[0]);
        //get events linked to this artist
        $this->registry->template->events = Event::getArtistEvents($pieces[0]);
        //get publications linked to this event
        $this->registry->template->pubs = Pub::getArtistPubs($pieces[0]);
        /*         * * load the page templates ** */
        $this->registry->template->show('page_head');
        $this->registry->template->show('artist_content');
        //$this->registry->template->show('artist_sidebar');
        $this->registry->template->show('footer');
    }

}
?>

