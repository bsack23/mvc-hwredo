<?php

Class staticController Extends baseController {

public function index() 
{
        $this->registry->template->static_heading = 'This is the static Index';
        $this->registry->template->show('static_index');
}


public function page($page_id = ''){

	/*** should not have to call this here.... FIX ME ***/

	$this->registry->template->static_heading = 'This is the ' . $page_id . ' heading';
	$this->registry->template->static_content = 'This is the ' . $page_id . ' heading';
        $this->registry->template->show('generic_header');
	$this->registry->template->show('static_page');
}

}
?>
