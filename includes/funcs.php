<?php

if (!function_exists('get_first_image')) {

    function get_first_image($html) {
        require_once('simple_html_dom.php');
        $post_html = str_get_html($html);
        if (!empty($post_html)) {
            $first_img = $post_html->find('img', 0);
        }
        if (!empty($first_img)) {
            return $first_img->src;
        } else {
            return null;
        }
    }

}

function set_imgurl($html_text) {
    $img_url = "http://www.hallwalls.org/" . getFirstImage($html_text);
}

function get_date($mysqldate, $just_date) {
    $phpdate = strtotime($mysqldate);
    if (isset($just_date)) {
        $date = date('M d', $phpdate);
    } else {
        $date = date('l, M d', $phpdate);
    }
    return $date;
}

function getFirstImage($html) {
    require_once('../classes/includes/simple_html_dom.php');
    $post_html = str_get_html($html);
    if (!empty($post_html)) {
        $first_img = $post_html->find('img', 0);
    }
    if (!empty($first_img)) {
        return $first_img->src;
    } else {
        return null;
    }
}

function myTruncate2($string, $limit, $break = ". ", $pad = " ... ") {
    // return with no change if string is shorter than $limit  
    if (strlen($string) <= $limit)
        return $string;
    $string = substr($string, 0, $limit);
    if (false !== ($breakpoint = strrpos($string, $break))) {
        $string = substr($string, 0, $breakpoint);
    } return $string . $pad;
}

function restoreTags($input) {
    $opened = array(); // loop through opened and closed tags in order  
    if (preg_match_all("/<(\/?[a-z]+)>?/i", $input, $matches)) {
        foreach ($matches[1] as $tag) {
            if (preg_match("/^[a-z]+$/i", $tag, $regs)) { // a tag has been opened  
                if (strtolower($regs[0]) != 'br')
                    $opened[] = $regs[0];
            } elseif (preg_match("/^\/([a-z]+)$/i", $tag, $regs)) { // a tag has been closed
                //unset($opened[array_pop(array_keys($opened, $regs[1]))]); 
                $some_var = array_keys($opened, $regs[1]);
                unset($opened[array_pop($some_var)]);
            }
        }
    } // close tags that are still open  
    if ($opened) {
        $tagstoclose = array_reverse($opened);
        foreach ($tagstoclose as $tag)
            $input .= "</$tag>";
    }
    return $input;
}// end restoreTags

if (!function_exists('fix_image_urls')) {
    function fix_image_urls($text) {
        $append_this = 'img src="'.ASSETS_PATH.'/';
        // to do: use preg_replace instead
        $output = str_replace('img src="', $append_this, $text);
        return $output;
    }
}
function set_description($description_text) {
    $description_text = preg_replace("/<img[^>]+\>/i", " <!--(image) --> ", $description_text);
    $description_text = preg_replace(" #<iframe.*?(/iframe>|$)#s", " <!--(iframe) --> ", $description_text);
    //$description = myTruncate2($description, $limit);
    $description = myTruncate2($description, 200);
    $description = restoreTags($description);
    $this->description = $description_text;
}

function make_short_description($description_text, $limit) {
    $description_text = preg_replace("/<img[^>]+\>/i", " <!--(image) --> ", $description_text);
    $description_text = preg_replace(" #<iframe.*?(/iframe>|$)#s", " <!--(iframe) --> ", $description_text);
    //$description = myTruncate2($description, $limit);
    $description = myTruncate2($description_text, $limit, ". ", " ...");
    $description = restoreTags($description);
    return $description;
}

if (!function_exists('format_title_artists')) {

    function format_title_artists($tit, $art, $strip = null) {
        $title = $tit;
        $artists = $art;
        $title_artists = "";
        if (strlen($artists) > 0)
            $title_artists = $artists;
        if ((strlen($artists) > 0) && (strlen($title) > 0))
            $title_artists = $title_artists . " - ";
        if (strlen($title) > 0)
            $title_artists = $title_artists . $title;
        if (!$strip) {
            return strip_tags($title_artists, '<i><em>');
        } else {
            return strip_tags($title_artists);
        }
    }

}

if (!function_exists('make_thumb')) {

    function make_thumb($filename, $directory_to_search, $width = 400, $height = 225) {
        if (!file_exists(ABS_ASSETS_PATH . "/thumbs/" . $filename)) {
            // if it doesn't exist, try to make one
            // if the file in the 'thumbnail' field of this event does exist
            if (file_exists(ABS_ASSETS_PATH . "/" .
                            $directory_to_search . "/" .
                            $filename)) {
                try {
                    //do magic things to it
                    $img = new abeautifulsite\SimpleImage(ABS_ASSETS_PATH .
                            "/" . $directory_to_search . "/" .
                            $filename);
                    // save it in 'thumbs' directory
                    $img->thumbnail($width, $height)->save(ABS_ASSETS_PATH . "/thumbs/" . $filename);
                } catch (Exception $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        }
    }

}
if (!function_exists('rewriter')) {

    function rewriter($rawroute) {
        $programs = ['visual', 'perflit', 'music', 'media-arts', 'special', 'community'];
        $searches = ['search'];
        $archives = ['artists', 'pubs'];
        $statics = ['about', 'history'];
        $front = ['autocomplete'];
        $outch = "";
        $wha = explode('/', $rawroute);
        if (count($wha) == 1) {
            // remove file extensions
            $tmp = pathinfo($wha[0], PATHINFO_FILENAME);
            if (in_array($tmp, $programs)) {
                $outch = "page/program/" . $tmp;
            } elseif (in_array($tmp, $statics)) {
                $outch = "static/page/" . $tmp;
            } elseif (in_array($tmp, $searches)) {
                $outch = "page/search";
            } else {
                $outch = "index";
            }
        } elseif (count($wha) == 2) {
            //if (in_array($wha[0], $programs)) {
            //debug_to_console("got here" . $wha[0] . $wha[1]);

            if ((in_array($wha[0], $programs)) || ($wha[0] == 'event')) {
                // remove file extensions
                $outch = "page/event/" . pathinfo($wha[1], PATHINFO_FILENAME);
            } elseif (in_array($wha[1], $front)) {
                $outch = "front/" . pathinfo($wha[1], PATHINFO_FILENAME);
            } elseif (in_array($wha[0], $archives)) {
                if ($wha[0] == "pubs") {
                    $outch = 'page/publication/' . pathinfo($wha[1], PATHINFO_FILENAME);
                }
            }
        } elseif (count($wha) == 3) {
            //debug_to_console("got here1: " . $wha[0] . " " . $wha[1] . " " . $wha[2]);
            if (in_array($wha[0], $archives)) {
                //debug_to_console("got here2: " . $wha[0] . " " . $wha[1] . " " . $wha[2]);
                if ($wha[0] == 'artists') {
                    //debug_to_console("got here3: " . $wha[0] . $wha[1] . $wha[2]);
                    $outch = "page/artist/" . pathinfo($wha[2], PATHINFO_FILENAME);
                }
            }
        } else {
            $outch = "index";
        }
        return($outch);
    }
}

function debug_to_console($data) {

    if (is_array($data)) {
        $output = "<script>console.log( 'Debug Objects: " . implode(',', $data) . "' );</script>";
    } else {
        $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
    }
    echo $output;
}

function getProgramNum($program_name) {
    switch ($program_name) {
        case 'visual': $num = 1;
            break;
        case 'perflit': $num = 2;
            break;
        case 'media-arts': $num = 3;
            break;
        case 'music': $num = 4;
            break;
        case 'community': $num = 5;
            break;
        case 'special': $num = 6;
            break;
        case 'calls': $num = 7;
            break;
        case 'perflit': $num = 8;
            break;
        default : $num = 1;
    }
    return $num;
}

// Returns only the file extension (without the period).
function file_ext($filename) {
    if (!preg_match('/./', $filename))
        return '';
    return preg_replace('/^.*./', '', $filename);
}

// Returns the file name, less the extension.
function file_ext_strip($filename) {
    return preg_replace('/.[^.]*$/', '', $filename);
}

function toAscii($str, $replace = array(), $delimiter = '-') {
    if (!empty($replace)) {
        $str = str_replace((array) $replace, ' ', $str);
    }
    $str = iconv("UTF-8", "ISO-8859-1", $str);
    $str = html_entity_decode($str, ENT_QUOTES, 'ISO-8859-1');
    $str = iconv("ISO-8859-1", "UTF-8", $str);
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
// $clean = iconv('ISO-8859-1', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}

?>

