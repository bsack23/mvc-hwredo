<?php
 /*** error reporting on ***/
 error_reporting(E_ALL);

 /*** define the site path ***/
 $site_path = realpath(dirname(__FILE__));
 define ('__SITE_PATH', $site_path);
 
define('PUBLIC_PATH',dirname(realpath(__FILE__)) . "/mvc-redo");
define('BASE_PATH',dirname($_SERVER['SCRIPT_NAME']));
define('ABS_ASSETS_PATH',__SITE_PATH . "/assets");
define('ASSETS_PATH',BASE_PATH . "/assets");
//define('LIB_PATH',BASE_PATH . "/lib/");

 /*** include the init.php file ***/
 include 'includes/init.php';
 
 /*** include some favorite functions ***/
 include 'includes/funcs.php';
 
 include 'classes/SimpleImage.class.php';

 /*** load the router ***/
 $registry->router = new router($registry);

 /*** set the controller path ***/
 $registry->router->setPath (__SITE_PATH . '/controller');

 /*** load up the template ***/
 $registry->template = new template($registry);

 /*** load the controller ***/
 $registry->router->loader();

?>
