<?php

class Artist {

    public $id;
    public $a_id;
    public $firstname;
    public $lastname;
    public $bio;
    public $img_url;
    public $artist_url;

    function __construct($artist_id, $alias_id, $artist_firstname, $artist_lastname) {
        $this->id = $artist_id;
        $this->a_id = $alias_id;
        $this->firstname = $artist_firstname;
        $this->lastname = $artist_lastname;
    }

    function setBio($b) {
        $this->bio = $b;
    }

    function setImg($i) {
        $this->img_url = $i;
    }
    
function setArtistURL($id, $fn, $ln) {
    $initial = strtoupper(substr($ln, 0, 1));
    $both_names = strtolower($fn) . "-" . strtolower($ln);
    $url = $initial . "/" . $id . "-" . $both_names;
    $this->artist_url = $url;
}

    public static function getAll() {
        $db = Db::getInstance();
        $query = "SELECT * FROM hallwall_org.artists WHERE "
                . "`flag_for_delete` != 1 "
                . "ORDER BY `lastname`";
        $result = $db->query($query);
        $artists = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $artists[$i] = new Artist($row["ID"], $row["a_id"], $row["name_first"], $row["name_last"]);
            $artists[$i]->setBio($row["bio"]);
            $artists[$i]->setImg($row["image_urls"]);
            $i++;
        }
        return $artists;
    }

    public static function getArtist($artist) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT * FROM hallwall_org.artists " .
                "WHERE artists.ID = ?"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }
        
        if (!$query->bind_param("s", $artist)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        
        //$artist = array();
        //$i = 0;
        while ($row = $result->fetch_assoc()) {
            $artist = new Artist($row["ID"], $row["a_id"], $row["name_first"], $row["name_last"]);
            $artist->setBio($row["bio"]);
            $artist->setImg($row["image_urls"]);
        }
        return $artist;
    }

    public static function getEventArtists($event) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT * FROM hallwall_org.artists " .
                "LEFT JOIN hallwall_org.EventsArtists ON (hallwall_org.EventsArtists.ArtistID=hallwall_org.artists.ID) " .
                "LEFT JOIN hallwall_org.events ON (hallwall_org.events.ID=hallwall_org.EventsArtists.EventID) " .
                "WHERE events.ID=? " .
                "GROUP BY hallwall_org.artists.ID " .
                "ORDER BY artists.name_last ASC"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }

        if (!$query->bind_param("s", $event)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $artists = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $artists[$i] = new Artist($row["ID"], $row["a_id"], $row["name_first"], $row["name_last"]);
            $artists[$i]->setBio($row["bio"]);
            $artists[$i]->setImg($row["image_urls"]);
            $artists[$i]->setArtistURL($row["a_id"], $row["name_first"], $row["name_last"]);
            $i++;
        }
        return $artists;
    }
    
    public static function getPubArtists($pub) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT artists.ID as aid, a_id as aa_id, name_last as name_last, " .
	 "name_first as name_first " .
	"FROM hallwall_org.artists " .
		"LEFT JOIN hallwall_org.ArtistsPubs ON (hallwall_org.ArtistsPubs.ArtistID=hallwall_org.artists.ID) " .
		"LEFT JOIN hallwall_org.publications ON (hallwall_org.publications.ID=hallwall_org.ArtistsPubs.PubID) " .
	"WHERE publications.ID=? " .
	"AND artists.flag_for_delete != '1'  " .	
	"GROUP BY hallwall_org.artists.ID " .
    "ORDER BY artists.name_last ASC"))) {
    echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }
        
        if (!$query->bind_param("s", $pub)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $artists = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $artists[$i] = new Artist($row["aid"], $row["aa_id"], $row["name_first"], $row["name_last"]);
            $artists[$i]->setArtistURL($row["aa_id"], $row["name_first"], $row["name_last"]);
            //$artists[$i]->setBio($row["bio"]);
            //$artists[$i]->setImg($row["image_urls"]);
            $i++;
        }
        return $artists;
    }
    
          

    public static function searchByArtist($keyword) {
        // this is used in the autocomplete search
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT * FROM hallwall_org.artists " .
                "WHERE artists.name_last LIKE CONCAT('%',?,'%') " .
                "OR artists.name_first LIKE CONCAT('%',?,'%') " .
                "ORDER BY artists.name_last ASC"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }
        
        if (!$query->bind_param("ss", $keyword, $keyword)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $artists = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $artists[$i] = new Artist($row["ID"], $row["a_id"], $row["name_first"], $row["name_last"]);
            $artists[$i]->setArtistURL($row["ID"], $row["name_first"], $row["name_last"]);
            $i++;
        }
        return $artists;
    }
}
