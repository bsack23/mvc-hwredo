<?php

class Event {
    public $DEV_DATE = 1;
    
    public $id;
    public $date;
    public $time;
    public $program;
    public $end_date;
    public $thumbnail;
    public $title;
    public $artist;
    public $presenter;
    public $location;
    public $description;
    public $short_desc;
    public $programtext;
    public $programimgdir;

    function __construct($event_id, $ev_date, $ev_time, $ev_pro) {
        $this->id = $event_id;
        $this->date = new DateTime($ev_date);
        //$this->date = $ev_date;
        $this->time = $ev_time;
        $this->program = $ev_pro;
    }

    function setEndDate($ed) {
        $this->end_date = new DateTime($ed);
    }

    function setTitle($t) {
        $this->title = $t;
    }

    function setArtist($a) {
        $this->artist = $a;
    }

    function setPresenter($pr) {
        $this->presenter = $pr;
    }

    function setLocation($loc) {
        $this->location = $loc;
    }

    function setDescription($desc) {
        $this->description = $desc;
    }

    function setShortDesc($short_desc) {
        $this->short_desc = $short_desc;
    }

    function setThumbnail($thumb) {
        $this->thumbnail = $thumb;
    }

    function setProgramText($pro) {
        switch ($pro) {
            case 1:
                $this->programtext = 'visual';
                break;
            case 2:
                $this->programtext = 'perflit';
                break;
            case 3:
                $this->programtext = 'media-arts';
                break;
            case 4:
                $this->programtext = 'music';
                break;
            case 5:
                $this->programtext = 'community';
                break;
            case 6:
                $this->programtext = 'special';
                break;
            case 7:
                $this->programtext = 'calls';
                break;
            case 8:
                $this->programtext = 'perflit';
                break;
            default:
                $this->programtext = 'none';
        }
    }

    function setProgramImgDir($pro) {
        switch ($pro) {
            case 1:
                $this->programimgdir = 'visual-images';
                break;
            case 2:
                $this->programimgdir = 'perf-lit-images';
                break;
            case 3:
                $this->programimgdir = 'media-arts-images';
                break;
            case 4:
                $this->programimgdir = 'music-images';
                break;
            case 5:
                $this->programimgdir = 'community-images';
                break;
            case 6:
                $this->programimgdir = 'special-images';
                break;
            case 7:
                $this->programimgdir = 'calls-images';
                break;
            case 8:
                $this->programimgdir = 'perf-lit-images';
                break;
            default:
                $this->programimgdir = 'none';
        }
    }

// http://requiremind.com/a-most-simple-php-mvc-beginners-tutorial/
    public static function getAll() {
        $db = Db::getInstance();
//        $query = "SELECT * FROM hallwall_org.events "
//                . "WHERE `event_date` >= CURDATE() "
//                . "AND `display_cal_only` != 1 AND `flag_for_delete` != 1 "
//                . "ORDER BY `event_date`";
        $query = "SELECT * FROM hallwall_org.events "
                . "WHERE `event_date` >= '2015-12-07' "
                . "AND `display_cal_only` != 1 AND `flag_for_delete` != 1 "
                . "ORDER BY `event_date`";
        $result = $db->query($query);
        $events = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $events[$i] = new Event($row["ID"], $row["event_date"], $row["event_time"], $row["program"]);
            $events[$i]->setEndDate($row["event_end_date"]);
            $events[$i]->setTitle($row["title"]);
            $events[$i]->setArtist($row["artist"]);
            $events[$i]->setPresenter($row["co_presenter"]);
            $events[$i]->setLocation($row["location"]);
            $events[$i]->setThumbnail($row["thumbnail"]);
            $events[$i]->setDescription($row["full_desc"]);
            $events[$i]->setShortDesc($row["short_desc"]);
            $events[$i]->setProgramText($row["program"]);
            $events[$i]->setProgramImgDir($row["program"]);
            $i++;
        }
        return $events;
    }

    public static function getGallery() {
        $db = Db::getInstance();
//        $query = "SELECT * FROM hallwall_org.events "
//                . "WHERE (`event_date` >= CURDATE() OR `event_end_date` >= CURDATE()) "
//                . "AND (`event_end_date` != '0000-00-00') "
//                . "AND (`program` = 1 OR `sec_program` = 1) "
//                . "AND (`display_cal_only` != 1) AND (`flag_for_delete` != 1) "
//                . "ORDER by `event_date` "
//                . "LIMIT 2";
        
        $query = "SELECT * FROM hallwall_org.events "
                . "WHERE (`event_date` >= '2015-12-07' OR `event_end_date` >= '2015-12-07') "
                . "AND (`event_end_date` != '0000-00-00') "
                . "AND (`program` = 1 OR `sec_program` = 1) "
                . "AND (`display_cal_only` != 1) AND (`flag_for_delete` != 1) "
                . "ORDER by `event_date` "
                . "LIMIT 2";
        $result = $db->query($query);
        $events = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $events[$i] = new Event($row["ID"], $row["event_date"], $row["event_time"], $row["program"]);
            $events[$i]->setEndDate($row["event_end_date"]);
            $events[$i]->setTitle($row["title"]);
            $events[$i]->setArtist($row["artist"]);
            $events[$i]->setPresenter($row["co_presenter"]);
            $events[$i]->setLocation($row["location"]);
            $events[$i]->setThumbnail($row["thumbnail"]);
            $events[$i]->setDescription($row["full_desc"]);
            $events[$i]->setShortDesc($row["short_desc"]);
            $events[$i]->setProgramText($row["program"]);
            $events[$i]->setProgramImgDir($row["program"]);
            $i++;
        }
        return $events;
    }

    public static function getBetween($begin, $end) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT * FROM hallwall_org.events "
                . "WHERE `event_date` BETWEEN ? AND ? "
                . "AND `display_cal_only` != 1 AND `flag_for_delete` != 1 "
                . "ORDER BY `event_date`"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }
        // bind parameters
        // 's' because it's a string
        if (!$query->bind_param("ss", $begin, $end)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $events = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $events[$i] = new Event($row["ID"], $row["event_date"], $row["event_time"], $row["program"]);
            $events[$i]->setEndDate($row["event_end_date"]);
            $events[$i]->setTitle($row["title"]);
            $events[$i]->setArtist($row["artist"]);
            $events[$i]->setPresenter($row["co_presenter"]);
            $events[$i]->setLocation($row["location"]);
            $events[$i]->setThumbnail($row["thumbnail"]);
            $events[$i]->setDescription($row["full_desc"]);
            $events[$i]->setShortDesc($row["short_desc"]);
            $events[$i]->setProgramText($row["program"]);
            $events[$i]->setProgramImgDir($row["program"]);
            // etc.
            $i++;
        }
        return $events;
    }

    public static function getTimemachine() {
        $db = Db::getInstance();
        $wb_date = date('d', time());
        $wb_month = date('m', time());
        $this_year = date('Y', time());
        $wb_go_backs = array(5, 10, 15, 20, 25, 30, 35, 40);
        //shuffle($wb_go_backs);
        $query = "SELECT * FROM hallwall_org.events 
            WHERE MONTH(event_date) = " . $wb_month
                . " AND DAYOFMONTH(event_date) = " . $wb_date
                . " AND (YEAR(event_date) = " . ($this_year - $wb_go_backs[0])
                . " OR YEAR(event_date) = " . ($this_year - $wb_go_backs[1])
                . " OR YEAR(event_date) = " . ($this_year - $wb_go_backs[2])
                . " OR YEAR(event_date) = " . ($this_year - $wb_go_backs[3])
                . " OR YEAR(event_date) = " . ($this_year - $wb_go_backs[4])
                . " OR YEAR(event_date) = " . ($this_year - $wb_go_backs[5])
                . " OR YEAR(event_date) = " . ($this_year - $wb_go_backs[6])
                . " OR YEAR(event_date) = " . ($this_year - $wb_go_backs[7])
                . ")";
        //echo $query;
        $result = $db->query($query);
        $events = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $events[$i] = new Event($row["ID"], $row["event_date"], $row["event_time"], $row["program"]);
            $events[$i]->setEndDate($row["event_end_date"]);
            $events[$i]->setTitle($row["title"]);
            $events[$i]->setArtist($row["artist"]);
            $events[$i]->setPresenter($row["co_presenter"]);
            $events[$i]->setLocation($row["location"]);
            $events[$i]->setThumbnail($row["thumbnail"]);
            $events[$i]->setDescription($row["full_desc"]);
            $events[$i]->setShortDesc($row["short_desc"]);
            $events[$i]->setProgramText($row["program"]);
            $events[$i]->setProgramImgDir($row["program"]);
            $i++;
        }
        return $events;
    }

    /* 	
      call it like this from a view script:
      $events = Event::getBetween($_POST['start'],$_POST['end']);
     */

    public static function getEvent($item) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT * FROM hallwall_org.events WHERE `ID` = ?"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }
        // bind parameters
        // 's' because it's a string
        if (!$query->bind_param("s", $item)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        //$events = array();
        //$i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $event = new Event($row["ID"], $row["event_date"], $row["event_time"], $row["program"]);
            $event->setEndDate($row["event_end_date"]);
            $event->setTitle($row["title"]);
            $event->setArtist($row["artist"]);
            $event->setPresenter($row["co_presenter"]);
            $event->setLocation($row["location"]);
            $event->setThumbnail($row["thumbnail"]);
            $event->setDescription($row["full_desc"]);
            $event->setShortDesc($row["short_desc"]);
            $event->setProgramText($row["program"]);
            $event->setProgramImgDir($row["program"]);
            // etc.
            //$i++;
        }
        return $event;
    }

    public static function getProgram($program_name) {
        $program_num = getProgramNum($program_name);
        $db = Db::getInstance();
//        if (!($query = $db->prepare("SELECT * FROM hallwall_org.events "
//                . "WHERE (`event_date` >= CURDATE() OR `event_end_date` >= CURDATE()) "
//                . "AND `program` = ? "
//                . "AND `display_cal_only` != 1 AND `flag_for_delete` != 1 "
//                . "ORDER BY `event_date`"))) {
//            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
//        }
        if (!($query = $db->prepare("SELECT * FROM hallwall_org.events "
                . "WHERE (`event_date` >= '2015-12-07' OR `event_end_date` >= '2015-12-07') "
                . "AND `program` = ? "
                . "AND `display_cal_only` != 1 AND `flag_for_delete` != 1 "
                . "ORDER BY `event_date`"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }
        if (!$query->bind_param("s", $program_num)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $events = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $events[$i] = new Event($row["ID"], $row["event_date"], $row["event_time"], $row["program"]);
            $events[$i]->setEndDate($row["event_end_date"]);
            $events[$i]->setTitle($row["title"]);
            $events[$i]->setArtist($row["artist"]);
            $events[$i]->setPresenter($row["co_presenter"]);
            $events[$i]->setLocation($row["location"]);
            $events[$i]->setThumbnail($row["thumbnail"]);
            $events[$i]->setDescription($row["full_desc"]);
            $events[$i]->setShortDesc($row["short_desc"]);
            $events[$i]->setProgramText($row["program"]);
            $events[$i]->setProgramImgDir($row["program"]);
            $i++;
        }
        return $events;
    }

    public static function getProgramTitle($pro) {
        $program_num = getProgramNum($pro);
        $title = '';
        switch ($program_num) {
            case 1:
                $title = 'Visual Arts';
                break;
            case 2:
                $title = 'Performance and Literature';
                break;
            case 3:
                $title = 'Media Arts';
                break;
            case 4:
                $title = 'Music';
                break;
            case 5:
                $title = 'Community';
                break;
            case 6:
                $title = 'Special';
                break;
            case 7:
                $title = 'Calls For Work';
                break;
            case 8:
                $title = 'Performance and Literature';
                break;
            default:
                $title = 'none';
        }
        return $title;
    }

    public static function getArtistEvents($artist) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT events.ID as ev_id, event_date, event_time, program, ".
                "title, artist, event_end_date " .
                "FROM hallwall_org.events " .
                "LEFT JOIN hallwall_org.EventsArtists ON (hallwall_org.EventsArtists.EventID=hallwall_org.events.ID) " .
                "LEFT JOIN hallwall_org.artists ON (hallwall_org.artists.ID=hallwall_org.EventsArtists.ArtistID) " .
                "WHERE artists.a_id=? " .
                "GROUP BY events.ID " .
                "ORDER BY events.event_date DESC"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }

        if (!$query->bind_param("s", $artist)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $events = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //debug_to_console($row["ev_id"]);
            $events[$i] = new Event($row["ev_id"], $row["event_date"], $row["event_time"], $row["program"]);
            $events[$i]->setEndDate($row["event_end_date"]);
            $events[$i]->setTitle($row["title"]);
            $events[$i]->setArtist($row["artist"]);
            $events[$i]->setProgramText($row["program"]);
            $events[$i]->setProgramImgDir($row["program"]);
            $i++;
        }
        return $events;
    }

}

//end class
?>