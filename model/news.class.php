<?php

class News {

    public $id;
    public $pulldate;
    public $imageURL;
    public $linkURL;
    public $headline;
    public $newstext;
    public $priority;

    function __construct($id, $pulldate, $imageURL, $linkURL, $headline, $newstext, $priority) {
        $this->id = $id;
        $this->pulldate = $pulldate;
        $this->imageURL = $imageURL;
        $this->linkURL = $linkURL;
        $this->headline = $headline;
        $this->newstext = $newstext;
        $this->priority = $priority;
    }

    public static function getAll() {
        $db = Db::getInstance();
        $query = "SELECT * FROM hallwall_org.news WHERE `pull_date` >= CURDATE() ORDER BY `priority`";
        $result = $db->query($query);
        $news = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $news[$i] = new News($row["ID"], $row["pull_date"], $row["image_url"], $row["link_url"], $row["headline"], $row["news_text"], $row["priority"]);
            $i++;
        }
        return $news;
    }

    /* 	
      call it like this from a controller script:
      $news = News::getAll();
     */
}

//end class
?>