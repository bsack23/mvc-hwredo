<?php

class Pub {

    public $id;
    public $title;
    public $year;
    public $ex_date;
    public $artists;
    public $curator;
    public $essayist;
    public $description;
    public $board_of_directors;
    public $staff;
    public $cover_image_caption;
    public $image_filebase;
    public $scribd_document_id;
    public $scribd_doc;
    public $scribd_access_key;
    public $category;
    public $tags;
    public $created_by;
    public $date_created;
    public $modified_by;
    public $date_modified;

    function __construct($id, $tit, $yr, $img, $ar, $desc, $cat) {
        $this->id = $id;
        $this->title = $tit;
        $this->year = $yr;
        $this->image_filebase = $img;
        $this->artists = $ar;
        $this->description = $desc;
        $this->category = $cat;
    }

    function setCurator($b) {
        $this->curator = $b;
    }

    function setEssayist($b) {
        $this->essayist = $b;
    }

    function setBoard($b) {
        $this->board_of_directors = $b;
    }

    function setCover($b) {
        $this->cover_image_caption = $b;
    }

    function setCreatedBy($b) {
        $this->created_by = $b;
    }

    function setDateCurated($b) {
        $this->date_created = $b;
    }

    function setModifiedBy($b) {
        $this->modified_by = $b;
    }

    function setDateModified($b) {
        $this->date_modified = $b;
    }

    function setExDate($b) {
        $this->ex_date = $b;
    }

    function setScribdAccessKey($b) {
        $this->scribd_access_key = $b;
    }

    function setScribdDoc($b) {
        $this->scribd_doc = $b;
    }

    function setScribdDocumentId($b) {
        $this->scribd_document_id = $b;
    }

    function setStaff($b) {
        $this->staff = $b;
    }

    function setTags($b) {
        $this->tags = $b;
    }

    public static function getPub($pub) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT publications.ID as pid, " .
                "publications.artists as partists, " .
                "publications.year as pyear, " .
                "publications.description as pdesc, " .
                "publications.category as pcat, " .
                "publications.image_filebase as pimage_filebase, " .
                "publications.title as ptitle, " .
                "publications.curator as curator, " .
                "publications.essayist as essayist, " .
                "publications.scribd_document_id as scribdid, " .
                "publications.scribd_doc as scribddoc, " .
                "publications.scribd_access_key as scribdkey, " .
                "publications.cover_image_caption as caption " .
                "from hallwall_org.publications " .
                "WHERE ID=?"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }

        if (!$query->bind_param("s", $pub)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }
        $result = $query->get_result();
        while ($row = $result->fetch_assoc()) {
            $pub = new Pub($row["pid"], $row["ptitle"], $row["pyear"], $row["pimage_filebase"], $row["partists"], $row["pdesc"], $row["pcat"]);
            $pub->setScribdAccessKey($row["scribdkey"]);
            $pub->setScribdDoc($row["scribddoc"]);
            $pub->setScribdDocumentId($row["scribdid"]);
            $pub->setEssayist($row["essayist"]);
            $pub->setCover($row["caption"]);
        }
        return $pub;
    }

    public static function getEventPubs($event) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT publications.ID as pid, " .
                "publications.artists as partists, " .
                "publications.year as pyear, " .
                "publications.description as pdesc, " .
                "publications.category as pcat, " .
                "publications.image_filebase as pimage_filebase, " .
                "publications.title as ptitle " .
                "FROM hallwall_org.publications
		LEFT JOIN hallwall_org.EventsPubs ON (hallwall_org.EventsPubs.PubID=hallwall_org.publications.ID)
		LEFT JOIN hallwall_org.events ON (hallwall_org.events.ID=hallwall_org.EventsPubs.EventID) " .
                "WHERE events.ID=? " .
                "GROUP BY hallwall_org.publications.ID " .
                "ORDER BY publications.year DESC"))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }

        if (!$query->bind_param("s", $event)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $pubs = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $pubs[$i] = new Pub($row["pid"], $row["ptitle"], $row["pyear"], $row["pimage_filebase"], $row["partists"], $row["pdesc"], $row["pcat"]);
            $i++;
        }
        return $pubs;
    }
    
    public static function getArtistPubs($artist) {
        $db = Db::getInstance();
        if (!($query = $db->prepare("SELECT publications.ID as pid, " .
	"publications.artists as partists, " .
	"publications.year as pyear, " .
	"publications.title as ptitle, " .
	"publications.image_filebase as pimage_filebase, " .
	"publications.category as pcat, " .
	"publications.description as pdesc " .
	"FROM hallwall_org.publications " .
		"LEFT JOIN hallwall_org.ArtistsPubs ON (hallwall_org.ArtistsPubs.PubID=hallwall_org.publications.ID) " .
		"LEFT JOIN hallwall_org.artists ON (hallwall_org.artists.ID=hallwall_org.ArtistsPubs.ArtistID) " .
	"WHERE artists.a_id=? " .
	"GROUP BY publications.ID " .
	"ORDER BY publications.year DESC"))){
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }

        if (!$query->bind_param("s", $artist)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }

        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();
        $pubs = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $pubs[$i] = new Pub($row["pid"], $row["ptitle"], $row["pyear"], $row["pimage_filebase"], $row["partists"], $row["pdesc"], $row["pcat"]);
            $i++;
        }
        return $pubs;
    }

}
