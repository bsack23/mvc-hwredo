<?php

class Search {

    public $id;
    public $type;

    public function __construct($id, $type, $text) {
        $this->id = $id;
        $this->type = $type;
        $this->text = $text;
    }

    public static function getEverything($terms) {
        $db = Db::getInstance();
        if (!($query = $db->prepare(
                "(SELECT a_id as id, CONCAT_WS(' ',name_first,name_last) as text, 'artist' as type FROM artists 
           WHERE CONCAT_WS(' ',name_first,name_last) LIKE CONCAT('%',?,'%')) 
           UNION (SELECT ID as id, CONCAT_WS(' ',title,artist) as text, 'event' as type FROM events WHERE MATCH
           (artist, title, short_desc, full_desc, co_presenter) AGAINST (? IN BOOLEAN MODE)) 
           UNION (SELECT ID as id, title as text, 'pub' as type FROM publications WHERE MATCH
           (artists, title, description) AGAINST (? IN BOOLEAN MODE))"
                ))) {
            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
        }
//        if (!($query = $db->prepare("(SELECT ID as id, 'event' as type FROM events WHERE title LIKE 
//            ? OR artist LIKE ? OR full_desc LIKE ?) 
//           UNION (SELECT a_id as id, 'artist' as type FROM artists WHERE CONCAT(name_first,' ',name_last) LIKE ?) 
//           UNION (SELECT ID as id, 'pub' as type FROM publications WHERE artists LIKE ? 
//           OR title LIKE ? OR description LIKE ?)"))) {
//            echo "Prepare failed: (" . $db->errno . ") " . $db->error;
//        }
        //$terms = '%' . $terms . '%';
        //if (!$query->bind_param("sssssss", $terms, $terms, $terms, $terms, $terms, $terms, $terms)) {
        
        //$terms = "\'" . $terms . "\'";
        $aterms = str_replace('\"',"",$terms);
        if (!$query->bind_param("sss", $terms,$aterms,$terms)) {
            echo "Binding parameters failed: (" . $query->errno . ") " . $query->error;
        }
    
        if (!$query->execute()) {
            echo "Execute failed: (" . $query->errno . ") " . $query->error;
        }

        $result = $query->get_result();

        $everything = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $everything[$i] = new Search($row["id"], $row["type"], $row["text"]);
            $i++;
        }
        return $everything;
    }

}
