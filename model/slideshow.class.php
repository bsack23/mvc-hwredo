<?php

class Slideshow {

    public $id;
    public $pulldate;
    public $imageURL;
    public $undertext;
    public $overtext;
    public $priority;

    function __construct($slide_id, $slide_pulldate, $slide_imageURL, $slide_undertext, $slide_overtext, $slide_priority) {
        $this->id = $slide_id;
        $this->pulldate = $slide_pulldate;
        $this->imageURL = $slide_imageURL;
        $this->undertext = $slide_undertext;
        $this->overtext = $slide_overtext;
        $this->priority = $slide_priority;
    }

    public static function getAll() {
        $db = Db::getInstance();
        $query = "SELECT * FROM hallwall_org.index_slideshow WHERE `pulldate` >= CURDATE() ORDER BY `priority`";
        $result = $db->query($query);
        $slides = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            //echo $row["ID"] . " " . $row["title"]. "<br>\n";
            $slides[$i] = new Slideshow($row["id"], $row["pulldate"], $row["imageURL"], $row["undertext"], $row["overtext"], $row["priority"]);
            $i++;
        }
        return $slides;
    }

    /* 	
      call it like this from a controller script:
      $slides = Slideshow::getAll();
     */
}

//end class
?>