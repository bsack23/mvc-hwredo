<body>
    <!-- to change: r&l padding on headleft & headright cols
    font-sizes on drop-down nav ie .top-nav > li a {font-size:12px}
    
    -->
    <div class="container"> <!--big container -->

        <div class="row"> <!--this is the header -->
            <div class="col headleft">
                <a href="../index"><img src="<?php echo ASSETS_PATH; ?>/main-images/banner_logo.png" alt="" ></a>

                <?php include $header_navselect; ?>
            </div>
            <div class="col headright">
                <div class="navbar">
                    <?php include $header_menu; ?>

                </div>        
            </div>

            <div class="row">
                <div class="col content">

                    <div class="breadcrumbs"><span><a href="archives">archives</a> > <a href="#">artists</a> ></span></div>


                    <div class="col description">
                        <h2 style="padding:.2em 0;"><?php echo $artist->firstname . " " . $artist->lastname; ?></h2>


                        <?php
                        if (count($events) > 0) {
                            //echo "COUNT:" . count($events);
                            echo "\n<div>&nbsp;</div>\n<h4>Some events that relate to this artist:</h4>\n" .
                            "<ul style=\"list-style:none;\">\n";
                            foreach ($events as $myevent) {
                                echo '<li class="' . $myevent->programtext .
                                '"><a href="' . BASE_PATH . '/' . $myevent->programtext . '/' . $myevent->id . '">'
                                . $myevent->date->format('n/d/y');
                                if ($myevent->end_date->format('Y-m-d') > '0000-00-00') {
                                    echo "&mdash;" . $myevent->end_date->format('n/d/y');
                                }
                                echo " - " .
                                format_title_artists($myevent->title, $myevent->artist) . "</a></li>\n";
                            }
                            echo "</ul>\n";
                        }
                        ?>

                        <?php
                        if (count($pubs) > 0) {
                            echo "COUNT:" . count($pubs);
                            echo "\n<div>&nbsp;</div>\n<h4>Some publications that relate to this artist:</h4>\n" .
                            "<ul style=\"list-style:none;\">\n";
                            foreach ($pubs as $this_pub) {
                                //echo "blah\n";
                            
                                echo "<li>";
                                       // "<img src=\"/pubs/" . $this_pub->image_filebase . ".TN.jpg\" alt=\"\" class=\"floatleft\" />" .
                                echo '<a href="' . BASE_PATH . '/' . 'pubs/' . $this_pub->id . "\">" . $this_pub->title . " - " .
                                $this_pub->year . "</a></li>\n";
                            }
                            echo "</ul>\n";
                        }
                        ?>
                        
                        <?php
//                        if (count($pubs) > 0) {
//                            //echo "COUNT:" . count($pubs);
//                            echo "\n<div>&nbsp;</div>\n<h4>Some publications that relate to this event:</h4>\n" .
//                            "<ul style=\"list-style:none;\">\n";
//                            foreach ($pubs as $this_pub) {
//                                //echo "blah\n";
//                            
//                                echo "<li>";
//                                       // "<img src=\"/pubs/" . $this_pub->image_filebase . ".TN.jpg\" alt=\"\" class=\"floatleft\" />" .
//                                echo "<a href=\"../pubs/" . $this_pub->id . "\">" . $this_pub->title . " - " .
//                                $this_pub->year . "</a></li>\n";
//                            }
//                            echo "</ul>\n";
//                        }
                        ?>

                    </div><!--end description -->

                </div><!--end content -->