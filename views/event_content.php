<body>
    <!-- to change: r&l padding on headleft & headright cols
    font-sizes on drop-down nav ie .top-nav > li a {font-size:12px}
    
    -->
    <div class="container"> <!--big container -->

        <div class="row"> <!--this is the header -->
            <div class="col headleft">
                <a href="../index"><img src="<?php echo ASSETS_PATH; ?>/main-images/banner_logo.png" alt="" ></a>

                <?php include $header_navselect; ?>
            </div>
            <div class="col headright">
                <div class="navbar">
                    <?php include $header_menu; ?>

                </div>        
            </div>

            <div class="row">
                <div class="col content">

                    <div class="breadcrumbs"><span><a href="#">calendar</a> > <a href="../<?php echo $event->programtext; ?>"><?php echo $event->programtext; ?></a></span></div>

                    <div class="col datetime">
                        <?php
                        echo $event->date->format('l') . "<br>" .
                        $event->date->format('F j, Y') . "<br>";

                        if ($event->end_date->format('Y') > 0) {
                            echo "through<br>" . $event->end_date->format('l') . "<br>" .
                            $event->end_date->format('F j, Y') . "<br>";
                        }

                        if (strlen($event->time) > 0) {
                            echo $event->time . "<br>";
                        }
                        if (strlen($event->location) > 0) {
                            echo $event->location . "<br>";
                        }
                        ?>
                        <!-- make the below disappear in mobile -->
                        <div class="share">
                            <br>
                            share on fb<br>add to calendar
                            <!--or at least move to below title,image,&text -->
                        </div>
                    </div>
                    <div class="col description">
                        <h2 style="padding:.2em 0;"><?php echo format_title_artists($event->title, $event->artist); ?></h2>
                        <?php
                        $new_description = $event->description;
                        if (count($artists) > 0) {
                            foreach ($artists as $this_artist) {
                                $initial = strtoupper($this_artist->lastname[0]);
                                $firstlast = trim(toAscii($this_artist->firstname . " " . $this_artist->lastname), "-");
                                $needle = "/(?!(?:[^<]+>|[^>]+<\/a>))\b(" .
                                        trim($this_artist->firstname) . " " .
                                        trim($this_artist->lastname) . ")\b/is";
                                $new_needle = "<a href=\"../artists/" . $initial . "/" .
                                        $this_artist->a_id . "-" . $firstlast . "\" class=\"uline\">" .
                                        $this_artist->firstname . " " . $this_artist->lastname
                                        . "</a>";
                                echo "<!-- <a href=\"../artists/" . $initial . "/" .
                                $this_artist->a_id . "-" . $firstlast . "\">" .
                                $this_artist->firstname . " " . $this_artist->lastname
                                . "</a>-->\n";
                                $new_description = preg_replace($needle, $new_needle, $new_description);
                            }
                        }
                        ?>
                        
                        <?php $newr_description = fix_image_urls($new_description);?>
                        
                        <?php echo $newr_description; ?>

                        <?php
                        if (count($pubs) > 0) {
                            //echo "COUNT:" . count($pubs);
                            echo "\n<div>&nbsp;</div>\n<h4>Some publications that relate to this event:</h4>\n" .
                            "<ul style=\"list-style:none;\">\n";
                            foreach ($pubs as $this_pub) {
                                //echo "blah\n";
                            
                                echo "<li>";
                                       // "<img src=\"/pubs/" . $this_pub->image_filebase . ".TN.jpg\" alt=\"\" class=\"floatleft\" />" .
                                echo "<a href=\"../pubs/" . $this_pub->id . "\">" . $this_pub->title . " - " .
                                $this_pub->year . "</a></li>\n";
                            }
                            echo "</ul>\n";
                        }
                        ?>

                    </div><!--end description -->

                </div><!--end content -->