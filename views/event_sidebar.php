<div class="col sidebar">
    <div class="row">
        <div class="col col-1-2">
            <div class="show show-third">
                <img src="<?php echo ASSETS_PATH; ?>/main-images/give.png" alt="">
                <div class="mask">
                    <p>Support Hallwalls through gifts or sponsorship.<br>
                        <a href="#" class="more">More info</a></p>
                </div>
            </div>
        </div>
        <div class="col col-1-2">
            <div class="show show-third">
                <img src="<?php echo ASSETS_PATH; ?>/main-images/join.png" alt="">
                <div class="mask">
                    <p>Support our programs and receive benefits!<br>
                        <a href="#" class="more">More info</a></p>
                </div>
            </div>
        </div>
    </div>

    <h2 class="mediumheading">related events</h2>
    <div class="row">
        <div class="col info">
            <a class="headanch" href="#"><h4>Artist and Title of Event</h4>
                <h5>Date of Event</h5></a>
            <img src="<?php echo ASSETS_PATH; ?>/thumbs/LightningLightningYES.gif" class="galleryimg"  alt="">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et m. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
        </div>

        <div class="col info">
            <a class="headanch" href="#"><h4>Artist and Title of Event</h4>
                <h5>Date of Event</h5></a>
            <img src="<?php echo ASSETS_PATH; ?>/thumbs/butler01-porchlight.jpg" class="galleryimg" alt="">
            <p>Lorem ipsum dolor sit amet, <a href="#">consectetuer adipiscing elit</a>. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et m. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
        </div>
    </div>    

    <?php include $social_crap; ?>

</div><!--end sidebar? -->