<ul class="top-nav">
    <li class="dropdown"><a href="#">about</a>
        <ul class="sub-nav">
            <li><a href="<?php echo BASE_PATH; ?>/history">history</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/staff">staff/board</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/submission">submission</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/opportunities">opportunities</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/harp">h.a.r.p.</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/directions">directions</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/rental">rental</a></li>
        </ul>
    </li>    
    <li class="dropdown"><a href="#">programs</a>
        <ul class="sub-nav">                    
            <li><a href="<?php echo BASE_PATH; ?>/media-arts">media&nbsp;arts</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/visual">visual&nbsp;arts</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/music">music</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/perflit">performance&nbsp;&amp;
                    literature</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/science-art">science&nbsp;&amp;
                    art&nbsp;cabaret</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/special">special&nbsp;events</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/community">community
                    events</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/drawing-rally">drawing&nbsp;rally</a></li>
            <li><a href="<?php echo BASE_PATH; ?>/hwtv">hwtv</a></li>
        </ul>
    </li>   
    <li><a href="#">calendar</a></li>
    <li class="dropdown"><a href="#">archive</a>
        <ul class="sub-nav">                    
            <li><a href="#">timeline</a></li>
            <li><a href="#">artists</a></li>
            <li><a href="#">publications</a></li>
            <li><a href="#">video collection</a></li>
        </ul>
    </li>
</ul>