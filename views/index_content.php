<body>

    <div class="container">
        <div class="row">
            <div class="navbar">
                <?php include $header_menu; ?>
            </div>         
        </div>
        <div class="row">
            <div class="col headleft">
                <img src="<?php echo ASSETS_PATH; ?>/main-images/banner_logo.png" alt="banner" >
                <?php include $header_navselect; ?>
            </div>
            <div class="col headright">
                <!--<nav> -->
                <!-- what are we going to put here? -->
                <!--</nav> -->
            </div>	    
        </div>
        <div class="row">
            <div class="col content">
                <!-- look at this: http://stackoverflow.com/questions/16482492/cycle2-make-images-links and http://jquery.malsup.com/cycle2/demo/non-image.php -->       
                <div class="cycle-slideshow"
                     data-cycle-timeout=0
                     data-cycle-caption="#adv-custom-caption"
                     data-cycle-overlay-template="{{cycleDesc}}"
                     data-cycle-caption-template="{{cycleTitle}}">
                    <div class="cycle-prev"></div>
                    <div class="cycle-next"></div>
                    <div class="cycle-overlay"></div>
                    <!-- 800 x 450 px -->
                    <!-- this div will be loaded dynamically too -->
                    <?php
                    //echo "\n\n";
                    foreach ($slides as $myslide) {
                        $imgurl = $myslide->imageURL;
                        $under = $myslide->undertext;
                        $over = $myslide->overtext;
                        echo "<img src=\"" . ASSETS_PATH . $myslide->imageURL . "\"" .
                        " alt=\"\" data-cycle-title=\"" . htmlentities($under) .
                        "\"\n data-cycle-desc=\"" . htmlentities($over) . "\">\n\n";
                    }
                    //echo "\n\n";
                    ?>
                    <!--<img src="<?php echo ASSETS_PATH; ?>/test_images/duke-battersby03.jpg" alt="" data-cycle-title="A &lt;a href=&quot;#&quot;&gt;Date&lt;/a&gt; and Time 1" 
                         data-cycle-desc="&lt;em&gt;Title 1&lt;/em&gt; and description of some kind">
                    <img src="<?php echo ASSETS_PATH; ?>/test_images/duke-battersby04.jpg" alt="" data-cycle-title="A Date and Time 2" 
                         data-cycle-desc="Title 2 and description of some kind">
                    <img src="<?php echo ASSETS_PATH; ?>/test_images/duke-battersby05.jpg" alt="" data-cycle-title="A Date and Time 3" 
                         data-cycle-desc="Title 3 and description of some kind">
                    <img src="<?php echo ASSETS_PATH; ?>/test_images/duke-battersby06.jpg" alt="" data-cycle-title="A Date and Time 4" 
                         data-cycle-desc="Title 4 and description of some kind">-->
                </div>
                <div id="adv-custom-caption" class="center"></div> 

                <!-- upcoming events -->
                <!--<div id="upcoming" style="height:10em;overflow:auto;"> -->
                <div style="height:1em;clear:both;">&nbsp;<!-- just a dirty spacer --></div>
                <div id="upcoming" >
                    <ul>
                        <!--http://code.google.com/p/jquery-nicescroll/
                        http://webdesign.tutsplus.com/articles/quick-tip-styling-scrollbars-to-match-your-ui-design-webdesign-9430
                        http://www.net-kit.com/jquery-custom-scrollbar-plugins/ -->

                        <?php
                        foreach ($events as $myevent) {
                            // echo $myevent->end_date->format('Y-m-d')."<br>";
                            if (($myevent->program == 1) && 
                                    ($myevent->end_date->format('Y-m-d') > '0000-00-00')) {
                                //echo "<li>nope</li>\n";
                                    }else{
                                echo "<li class=\"" . $myevent->programtext .
                                "\"><a href=" . $myevent->programtext . "/" . $myevent->id . ">"
                                . $myevent->date->format('n/d');
                                if ($myevent->end_date->format('Y-m-d') > '0000-00-00') {
                                    echo "&mdash;" . $myevent->end_date->format('n/d');
                                }
                                echo " - " .
                                format_title_artists($myevent->title, $myevent->artist) . "</a></li>\n";
                            }
                        }
                        ?>
                    </ul>
                </div><!-- /upcoming -->

                <!-- load these as needed for news, etc. -->
                <div class="row">
                    <?php
                    if (count($news) > 0) {
                        $news_i = count($news);
                        $prev_i = 0;
                        foreach ($news as $news_item) {
                            if (($news_i >= 2)||($prev_i % 2 == 1)) {
                                echo "<div class=\"col feature\">\n";
                            } else {
                                echo "<div class=\"col feature-1\">\n";
                            }
                            // do things here
                            if(strlen($news_item->linkURL) > 0) {
                             echo "<a href=\"".$news_item->linkURL."\">\n";   
                            }
                            if(strlen($news_item->imageURL) > 0) {
                                echo "<img src=\"".ASSETS_PATH.$news_item->imageURL."\" alt=\"\" class=\"galleryimg\">\n";
                            }
                            echo "<h3 class=\"mediumheading\">".$news_item->headline."</h3>\n";
                            if(strlen($news_item->linkURL) > 0) {
                             echo "</a>";   
                            }
                            echo "<p>".$news_item->newstext."</p>\n";
                            echo "</div>\n";
                            --$news_i;
                            ++$prev_i;
                        }
                    }
                    ?>
<!--                    <div class="col feature">
                        <h4 class="mediumheading">Feature Block One</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et m. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                    </div>

                    <div class="col feature">
                        <h4 class="mediumheading">Feature Block Two</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et m. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                    </div>

                    <div class="col feature-1">
                        <h3 class="mediumheading">one-column version</h3>
                        <p>Cum sociis natoque penatibus et m. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et m. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                    </div>-->


                </div><!-- /row -->
            </div><!--/content-->