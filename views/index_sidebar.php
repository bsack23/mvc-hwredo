<div class="col sidebar">

    <div class="row">
        <div class="col-1-2">
            <div class="show show-third">
                <img src="<?php echo ASSETS_PATH; ?>/main-images/give.png" alt="">
                <div class="mask">
                    <p>Support Hallwalls through gifts or sponsorship.<br>
                        <a href="#" class="more">More info</a></p>
                </div>
            </div>
        </div>
        <div class="col-1-2">
            <div class="show show-third">
                <img src="<?php echo ASSETS_PATH; ?>/main-images/join.png" alt="">
                <div class="mask">
                    <p>Support our programs and receive benefits!<br>
                        <a href="#" class="more">More info</a></p>
                </div>
            </div>
        </div>
    </div>
    <h2 class="mediumheading">in the gallery</h2>
    <div class="row">
        <?php
        $num_galleries = count($gallery);
//$g_date_flag = 1;
        $g_prev_date = 'null';
        $g_prev_enddate = 'null';
//$i_g = 0;
//$g_stop = 0;
        foreach ($gallery as $g_event) {
            $g_artist = $g_event->artist;
            $g_title = $g_event->title;
            if ($num_galleries == 1) {
                echo "<div class=\"col info-1\">\n";
            } else {
                echo "<div class=\"col info\">\n";
            }
            if ($g_prev_date != $g_event->date->format('Y-m-d')) {
                // if two shows are on the same date only show date once
                echo "<h5 class=\"mediumheading\">";
                // if a show isn't open yet say 'opening', otherwise...
                if ($g_event->date->format('Y-m-d') >= date('Y-m-d')) {
                    echo "Opening " . $g_event->date->format('D., n/d');
                } else {
                    echo "Through " . $g_event->end_date->format('D., n/d');
                }
                echo "</h5>\n";
            }
            echo "<a class=\"headanch\" href=" . $g_event->programtext . "/" . $g_event->id . ">\n";
            if (strlen($g_artist) > 0) {
                echo "<h4>" . $g_artist . "</h4>";
                if (strlen($g_title) > 0) {
                    echo "<h5>" . $g_title . "</h5>";
                }
            } else {
                echo "<h4>" . $g_title . "</h4>";
            }

            echo "</a>\n";
            make_thumb($g_event->thumbnail, $g_event->programimgdir);
            echo "<img src=\"" . ASSETS_PATH . "/thumbs/" . $g_event->thumbnail . "\" class=\"galleryimg\" alt=\"\">\n";
            echo "<p>" . make_short_description($g_event->short_desc, 250) . "</p>\n";
            echo "</div>\n\n";

            $g_prev_date = $g_event->date->format('Y-m-d');
        }
        ?>
    </div><!-- close row -->
    <!-- end of gallery -->
    <div class="row">
        <?php
        if (count($timemachine) > 0) {
            $tm_event = $timemachine[rand(0, (count($timemachine) - 1))];
            echo "
        <div class=\"col info-1\">\n"
            . "<h2 class=\"mediumheading\">from the archive</h2>\n"
            . "<h5>" . $tm_event->date->format('D., M. j, Y') . "</h5>\n"
            . "<h4><a href=" . $tm_event->programtext . "/" . $tm_event->id . ">\n"
            . format_title_artists($tm_event->title, $tm_event->artist) . "</a></h4>\n"
            . "<p>" . make_short_description($tm_event->short_desc, 250) . "</p>\n"
            . "</div>\n";
        }
        ?>
    </div><!-- /row -->
    <!-- social crap here -->
    <?php include $social_crap; ?>
</div><!--end sidebar -->
