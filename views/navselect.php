<nav>
    <select class="navselect" onChange="location = this.options[this.selectedIndex].value;">
        <option selected="selected" value="">&#9776; MENU</option>
        <option value="index.html">home</option>
        <optgroup label="about">
            <option value="#">history</option>
            <option value="#">staff/board</option>
            <option value="#">submission</option>
            <option value="#">opportunities</option>
            <option value="#">h.a.r.p.</option>
            <option value="#">directions</option>
            <option value="#">rental</option>
        </optgroup>
        <optgroup label="programs">
            <option value="<?php echo BASE_PATH; ?>media-arts">media&nbsp;arts</option>
            <option value="<?php echo BASE_PATH; ?>visual">visual&nbsp;arts</option>
            <option value="<?php echo BASE_PATH; ?>music">music</option>
            <option value="<?php echo BASE_PATH; ?>perflit">performance&nbsp;&amp;
                literature</option>
            <option value="<?php echo BASE_PATH; ?>science-art">science&nbsp;&amp;
                art&nbsp;cabaret</option>
            <option value="<?php echo BASE_PATH; ?>special">special&nbsp;events</option>
            <option value="<?php echo BASE_PATH; ?>community">community
                events</option>
            <option value="<?php echo BASE_PATH; ?>drawing-rally">drawing&nbsp;rally</option>
            <option value="<?php echo BASE_PATH; ?>hwtv">hwtv</option>
        </optgroup>
        <option value="#">calendar</option>
        <optgroup label="archive">      
            <option value="#">timeline</option>
            <option value="#">artists</option>
            <option value="#">publications</option>
            <option value="#">video collection</option> 
        </optgroup>        
    </select> 
</nav>
