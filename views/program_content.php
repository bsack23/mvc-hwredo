<body>
    <!-- to change: r&l padding on headleft & headright cols
    font-sizes on drop-down nav ie .top-nav > li a {font-size:12px}
    
    -->
    <div class="container"> <!--big container -->

        <div class="row"> <!--this is the header -->
            <div class="col headleft">
                <a href="index"><img src="<?php echo ASSETS_PATH; ?>/main-images/banner_logo.png" alt="" ></a>

                <?php include $header_navselect; ?>
            </div>
            <div class="col headright">
                <div class="navbar">
                    <?php include $header_menu; ?>

                </div>        
            </div>

            <div class="row">
                <div class="col content">
                    <div class="col description">
                        <div class="breadcrumbs">
                            <span><a href="#">programs</a> > <a href="#"><?php echo $title; ?></a></span></div> 
                        <?php
                        if (count($program) == 0) {
                            echo "<h3 class=\"mediumheading\">There are no upcoming " . $title . " events at this time. Please sign up for our email newsletter and check our calendar for other events</h3>";
                        } else {
                            echo "<h2 class=\"mediumheading\">current and upcoming " . $title . " events</h2>\n
    <div class=\"row\">\n";
                            $num_events = count($program);
                            $up_counter = 1;
                            $down_counter = $num_events;
                            //if (($num_events % 2) == 0) {
                            //    $event_counter = $num_events;
                            //}
                            $p_prev_date = 'null';
                            $p_prev_enddate = 'null';
                            foreach ($program as $p_event) {
                                $p_artist = $p_event->artist;
                                $p_title = $p_event->title;
                                if (($down_counter == 1)&&(($up_counter % 2) != 0)) {
                                    // check for one that should be on it's own row
                                    echo "<div class=\"col info-1\">\n";
                                } else {
                                    echo "<div class=\"col info\">\n";
                                }
                                
                                //echo "up:" . $up_counter . "down:" . $down_counter;
                                if ($p_event->end_date->format('Y') <= 0) {
                                    //do the usual here
                                    //echo "yep, it works " . $p_event->end_date->format('Y-m-d');
                                    echo "<h5 class=\"mediumheading\">";
                                    echo $p_event->date->format('D., n/d') . " " . $p_event->time;
                                    echo "</h5>\n";
                                    
                                }
                                //elseif ($p_prev_date != $p_event->date->format('Y-m-d')) 
                                    else {
                                    // if two shows are on the same date only show date once
                                    echo "<h5 class=\"mediumheading\">";
                                    // if a show isn't open yet say 'opening', otherwise...
                                    if ($p_event->date->format('Y-m-d') >= date('Y-m-d')) {
                                        echo "Opening " . $p_event->date->format('D., n/d');
                                    } else {
                                        echo "Through " . $p_event->end_date->format('D., n/d');
                                    }
                                    echo "</h5>\n";
                                }
                                echo "<a class=\"headanch\" href=" . $p_event->programtext . "/" . $p_event->id . ">\n";
                                if (strlen($p_artist) > 0) {
                                    echo "<h4>" . $p_artist . "</h4>";
                                    if (strlen($p_title) > 0) {
                                        echo "<h5>" . $p_title . "</h5>";
                                    }
                                } else {
                                    echo "<h4>" . $p_title . "</h4>";
                                }

                                echo "</a>\n";
                                make_thumb($p_event->thumbnail, $p_event->programimgdir);
                                echo "<img src=\"" . ASSETS_PATH . "/thumbs/" . $p_event->thumbnail 
                                        . "\" class=\"galleryimg\" alt=\"\">\n";
                                echo "<p>" 
                                . make_short_description(strip_tags($p_event->description,'<b><em><i><strong>'), 250) 
                                . "</p>\n";
                                echo "</div>\n";//closing col info

                                $p_prev_date = $p_event->date->format('Y-m-d');
                                
                                //if($event_counter)
                                //++$up_counter;
                                --$down_counter;
                                
                                if((($up_counter % 2) == 0) && ($down_counter > 0)) {
                                    echo "\n</div>\n<div class=\"row\">\n";
                                } elseif ($down_counter == 0) {
                                    echo "\n</div>\n";
                                }
                                ++$up_counter;
                            }
                        }
                        ?>
                    
                    <!-- end of gallery -->


                </div><!--end description -->

            </div><!--end content -->