<body>
    <!-- to change: r&l padding on headleft & headright cols
    font-sizes on drop-down nav ie .top-nav > li a {font-size:12px}
    
    -->
    <div class="container"> <!--big container -->

        <div class="row"> <!--this is the header -->
            <div class="col headleft">
                <a href="index"><img src="<?php echo ASSETS_PATH; ?>/main-images/banner_logo.png" alt="" ></a>

                <?php include $header_navselect; ?>
            </div>
            <div class="col headright">
                <div class="navbar">
                    <?php include $header_menu; ?>

                </div>        
            </div>

            <div class="row">
                <div class="col content">
                    <div class="col description">
                        <div class="breadcrumbs">
                            <span><a href="#">publications</a> ></span>
                        </div> 

                        <h3 class="mediumheading">
                            <?php 
                            if ($pub->category == 3) {
                                echo 'Hallwalls Calendar: ';
                            }
                                
                            echo $pub->title; 
                            ?></h3>
                        <?php
                        if ((strlen($pub->year) > 0) && ($pub->category != 3)) {
                            echo "<strong>Published in " . $pub->year . "</strong><br />";
                        }
                        ?>
                        <div style="height: 1em;">&nbsp;</div>
                        <?php
                        $new_artists = $pub->artists;
                        if (count($artists) > 0) {
                            foreach ($artists as $this_artist) {
                                $initial = strtoupper($this_artist->lastname[0]);
                                $firstlast = trim(toAscii($this_artist->firstname . " " . $this_artist->lastname), "-");
                                $needle = "/(?!(?:[^<]+>|[^>]+<\/a>))\b(" .
                                        trim($this_artist->firstname) . " " .
                                        trim($this_artist->lastname) . ")\b/is";
                                $new_needle = "<a href=\"../artists/" . $this_artist->artist_url . "\" class=\"uline\">" .
                                        $this_artist->firstname . " " . $this_artist->lastname
                                        . "</a>";
                                echo "<!-- <a href=\"../artists/" . $initial . "/" .
                                $this_artist->a_id . "-" . $firstlast . "\">" .
                                $this_artist->firstname . " " . $this_artist->lastname
                                . "</a>-->\n";
                                $new_artists = preg_replace($needle, $new_needle, $new_artists);
                            }
                            echo "<strong>Artists associated with this publication:</strong><br />\n";
                            echo $new_artists;
                        }
                            ?>
                        
                    </div><!--end description -->

                </div><!--end content -->