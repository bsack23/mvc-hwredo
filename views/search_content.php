<body>
    <!-- to change: r&l padding on headleft & headright cols
    font-sizes on drop-down nav ie .top-nav > li a {font-size:12px}
    
    -->
    <div class="container"> <!--big container -->

        <div class="row"> <!--this is the header -->
            <div class="col headleft">
                <a href="index"><img src="<?php echo ASSETS_PATH; ?>/main-images/banner_logo.png" alt="" ></a>

                <?php include $header_navselect; ?>
            </div>
            <div class="col headright">
                <div class="navbar">
                    <?php include $header_menu; ?>

                </div>        
            </div>

            <div class="row">
                <div class="col content">
                    <div class="col description">
                        <div class="breadcrumbs">
                            <span><a href="#">archive</a> > <a href="search">search</a></span></div> 
                        <form action="" method="POST">
                            <input type="text" name="search_string">
                            <input type="submit" name="submit">
                        </form>
                        <?php
                        if (isset($results)) {
                            if (count($results) == 0) {
                                echo "<h2 class=\"mediumheading\">Your search did not return any results. Please adjust your search terms and try again.</h2>";
                            } else {
                                $num_results = count($results);
                                echo "<h2 class=\"mediumheading\">Your search found " . $num_results . " results</h2>\n
    <!--<div class=\"row\">-->\n<ul>";

                                $prev_type = "";
                                foreach ($results as $result) {
                                    if ($result->type != $prev_type) {
                                        echo "</ul>\n<h3 class=\"mediumheading\">" . $result->type . "</h3>\n<ul>";
                                    }
                                    echo "<li><a href=\"" . $result->type . "/" .
                                    $result->id . "\">" . $result->text . "</a></li>\n";
                                    $prev_type = $result->type;
                                }
                                echo "</ul><!--</div>-->";
                            }
                        }
                        ?>
                        <div style="height: 1em;">&nbsp;</div>
                       <input type="text" value="" placeholder="Search" id="keyword">
                       <div id="results">
                           
                       </div>

                        <div style="height: 10em;">&nbsp;</div>
                        
                    </div><!--end description -->

                </div><!--end content -->